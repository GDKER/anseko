package org.i9.slb.platform.anseko.provider.service;

import org.i9.slb.platform.anseko.provider.IDubboInstanceRemoteService;
import org.i9.slb.platform.anseko.provider.dto.InstanceDto;
import org.i9.slb.platform.anseko.provider.repository.InstanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 实例远程调用服务类
 *
 * @author R12
 * @version 1.0
 * @date 2018/9/4 10:41
 */
@Service("dubboInstanceRemoteService")
public class DubboInstanceRemoteService implements IDubboInstanceRemoteService {

    @Autowired
    private InstanceRepository instanceRepository;

    /**
     * 获取实例信息
     *
     * @param id
     * @return
     */
    @Override
    public InstanceDto getInstanceDtoInfo(String id) {
        InstanceDto instanceDto = this.instanceRepository.getInstanceDtoInfo(id);
        return instanceDto;
    }

    /**
     * 获取实例列表
     *
     * @return
     */
    @Override
    public List<InstanceDto> getInstanceDtoList() {
        List<InstanceDto> list = this.instanceRepository.getInstanceDtoList();
        return list;
    }

    /**
     * 创建实例
     *
     * @param instanceDto
     */
    @Override
    public void createInstanceInfo(InstanceDto instanceDto) {
        this.instanceRepository.insertInstanceInfo(instanceDto);
    }
}
